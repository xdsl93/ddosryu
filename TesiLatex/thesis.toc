\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{1}{chapter.1}
\contentsline {paragraph}{Obiettivi della tesi}{2}{section*.5}
\contentsline {chapter}{\numberline {2}Software-Defined Network}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Controller}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Application Plane}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Control Plane}{8}{section.2.3}
\contentsline {section}{\numberline {2.4}Data Plane}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}Northbound API}{10}{section.2.5}
\contentsline {section}{\numberline {2.6}Southbound API}{11}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}OpenFlow}{11}{subsection.2.6.1}
\contentsline {subsubsection}{Forwarding table}{12}{section*.6}
\contentsline {subsubsection}{Messaggistica}{13}{section*.7}
\contentsline {chapter}{\numberline {3}Analisi del progetto}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Analisi del problema}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Metodologie per la rilevazione}{19}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Metodologie per la mitigazione}{21}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Requisiti del progetto}{22}{section.3.2}
\contentsline {chapter}{\numberline {4}Progetto ed implementazione}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Progetto del sistema secondo architettura MAPE-K}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Implementazione}{29}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Esempi di codice}{31}{subsection.4.2.1}
\contentsline {chapter}{\numberline {5}Test e validazione}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}Test di studio}{35}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Caso 1}{35}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Caso 2}{35}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Caso 3}{36}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Caso 4}{36}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}Validazione}{37}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Caso 1}{37}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Caso 2}{38}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Caso 3}{39}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Caso 4}{40}{subsection.5.2.4}
\contentsline {chapter}{\numberline {6}Conclusioni}{41}{chapter.6}
